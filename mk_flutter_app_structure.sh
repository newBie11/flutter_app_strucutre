#!/bin/bash


DESTINATION=$1

if [ -z  "$1" ]
then
echo "Error | please provide an entry point"
exit -1
fi


rm -r "${DESTINATION}/lib"
rm -r "${DESTINATION}/test"

mkdir -p "${DESTINATION}/lib"
mkdir -p "${DESTINATION}/test"

# Creating assets folder
mkdir -p "${DESTINATION}/assets"
mkdir -p "${DESTINATION}/assets/fonts"
mkdir -p "${DESTINATION}/assets/html"
mkdir -p "${DESTINATION}/assets/i18n"
mkdir -p "${DESTINATION}/assets/images"



# LIB FOLDER
# Config folder 
mkdir -p "${DESTINATION}/lib/config"
mkdir -p "${DESTINATION}/lib/config/routes"
touch "${DESTINATION}/lib/config/routes/fluro_package_for_routing"
mkdir -p "${DESTINATION}/lib/config/themes"

# Constants 
mkdir -p "${DESTINATION}/lib/constants"
touch "${DESTINATION}/lib/constants/api_path.dart"
touch "${DESTINATION}/lib/constants/api_constants.dart"
touch "${DESTINATION}/lib/constants/asset_path.dart"


# Custom widgts
mkdir -p "${DESTINATION}/lib/widgets"

# Utils
mkdir -p "${DESTINATION}/lib/utils"
# Note: All the services will be singleton classes.
mkdir -p "${DESTINATION}/lib/utils/services"
touch "${DESTINATION}/lib/utils/services/local_storage_service.dart"
touch "${DESTINATION}/lib/utils/services/secure_storage_service.dart"
touch "${DESTINATION}/lib/utils/services/rest_api_service.dart"
touch "${DESTINATION}/lib/utils/services/native_api_service.dart"
mkdir -p "${DESTINATION}/lib/utils/ui"
mkdir -p "${DESTINATION}/lib/utils/ui/animation"
touch "${DESTINATION}/lib/utils/ui/app_dialogs.dart"
touch "${DESTINATION}/lib/utils/ui/ui_utils.dart"
mkdir -p "${DESTINATION}/lib/utils/mixins"

# Core
mkdir -p "${DESTINATION}/lib/core"
mkdir -p "${DESTINATION}/lib/core/auth"
mkdir -p "${DESTINATION}/lib/core/auth/login"
mkdir -p "${DESTINATION}/lib/core/auth/register"
mkdir -p "${DESTINATION}/lib/core/auth/forget_password"
mkdir -p "${DESTINATION}/lib/core/settings"
mkdir -p "${DESTINATION}/lib/core/walk_through"

# Modules
mkdir -p "${DESTINATION}/lib/modules"
mkdir -p "${DESTINATION}/lib/modules/generic_feature_name"
mkdir -p "${DESTINATION}/lib/modules/generic_feature_name/bloc"
mkdir -p "${DESTINATION}/lib/modules/generic_feature_name/models"
mkdir -p "${DESTINATION}/lib/modules/generic_feature_name/reposotories"
mkdir -p "${DESTINATION}/lib/modules/generic_feature_name/screens"


# TEST FOLDER
# Config folder 
mkdir -p "${DESTINATION}/test/config"
mkdir -p "${DESTINATION}/test/config/routes"
touch "${DESTINATION}/test/config/routes/fluro_package_for_routing"
mkdir -p "${DESTINATION}/test/config/themes"

# Constants 
mkdir -p "${DESTINATION}/test/constants"
touch "${DESTINATION}/test/constants/api_path.dart"
touch "${DESTINATION}/test/constants/api_constants.dart"
touch "${DESTINATION}/test/constants/asset_path.dart"


# Custom widgts
mkdir -p "${DESTINATION}/test/widgets"

# Utils
mkdir -p "${DESTINATION}/test/utils"
# Note: All the services will be singleton classes.
mkdir -p "${DESTINATION}/test/utils/services"
touch "${DESTINATION}/test/utils/services/local_storage_service.dart"
touch "${DESTINATION}/test/utils/services/secure_storage_service.dart"
touch "${DESTINATION}/test/utils/services/rest_api_service.dart"
touch "${DESTINATION}/test/utils/services/native_api_service.dart"
mkdir -p "${DESTINATION}/test/utils/ui"
mkdir -p "${DESTINATION}/test/utils/ui/animation"
touch "${DESTINATION}/test/utils/ui/app_dialogs.dart"
touch "${DESTINATION}/test/utils/ui/ui_utils.dart"
mkdir -p "${DESTINATION}/test/utils/mixins"

# Core
mkdir -p "${DESTINATION}/test/core"
mkdir -p "${DESTINATION}/test/core/auth"
mkdir -p "${DESTINATION}/test/core/auth/login"
mkdir -p "${DESTINATION}/test/core/auth/register"
mkdir -p "${DESTINATION}/test/core/auth/forget_password"
mkdir -p "${DESTINATION}/test/core/settings"
mkdir -p "${DESTINATION}/test/core/walk_through"

# Modules
mkdir -p "${DESTINATION}/test/modules"
mkdir -p "${DESTINATION}/test/modules/generic_feature_name"
mkdir -p "${DESTINATION}/test/modules/generic_feature_name/bloc"
mkdir -p "${DESTINATION}/test/modules/generic_feature_name/models"
mkdir -p "${DESTINATION}/test/modules/generic_feature_name/reposotories"
mkdir -p "${DESTINATION}/test/modules/generic_feature_name/screens"

